﻿/* Practica 1 */

-- 1. Mostrar todos los campos y todos los registros de la tabla empleado
SELECT
 * 
FROM
 emple e
;

-- 2. Mostrar todos los campos y todos los registros de la tabla departamento
SELECT
 * 
FROM
 depart d
;

-- 3. Mostrar el apellido y oficio de cada empleado
SELECT
 e.apellido, e.oficio 
FROM
 emple e
;

-- 4. Mostrar localizacion y numero de cada departamento
SELECT
 d.loc, d.dept_no 
FROM
 depart d
;

-- 5. Mostrar el numero, nombre y localizacion de cada departamento
SELECT
 d.dept_no, d.dnombre, d.loc 
FROM
 depart d
;

-- 6. Indicar el numero de empleados que hay
SELECT
 COUNT(*) 
FROM
 emple e
;

-- 7. Datos de los empleados ordenados por apellido de forma ascendente
SELECT
 * 
FROM
 emple 
ORDER BY apellido
;

-- 8. Datos de los empleados ordenados por apellido de forma descendente
SELECT
 * 
FROM
 emple 
ORDER BY apellido DESC
;

-- 9. Indicar el numero de departamentos que hay
SELECT
 COUNT(*)
FROM
 depart
;

-- 10. Indicar el numero de empleados mas el numero de departamentos
SELECT
(SELECT COUNT(*) FROM emple)
+
(SELECT COUNT(*) FROM depart)
;

-- 11. Datos de los empleados ordenados por numero de departamento descendentemente
SELECT
 * 
FROM
 emple 
ORDER BY dept_no DESC
;

-- 12. Datos de los empleados ordenados por numero de departamento descendentemente y por oficio ascendente
SELECT
 * 
FROM
 emple 
ORDER BY dept_no DESC, oficio
;

-- 13. Datos de los empleados ordenados por numero de departamento descendentemente y por apellido ascendentemente
SELECT
 * 
FROM
 emple 
ORDER BY dept_no DESC, apellido
;

-- 14. Mostrar los codigos de los empleados cuyo salario sea mayor que 2000
SELECT
 emp_no 
FROM
 emple
WHERE 
salario>2000
;

-- 15. Mostrar los codigos y apellidos de los empleados cuyo salario sea menor de 2000
SELECT
 emp_no, apellido 
FROM
 emple
WHERE 
salario<2000
;

-- 16. Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500
SELECT
 *
FROM
 emple
WHERE 
salario BETWEEN 1500 AND 2500
;

-- 17. Mostrar los datos de los empleados cuyo oficio sea 'ANALISTA'
SELECT
 *
FROM
 emple
WHERE 
oficio LIKE 'ANALISTA'
;

-- 18. Mostrar los datos de los empleados cuyo oficio sea 'ANALISTA' y ganen mas de 2000 €
SELECT
 *
FROM
 emple
WHERE 
 oficio LIKE 'ANALISTA'
  AND
 salario>2000
;

-- 19. Seleccionar el apellido y oficio de los empleados del departamento 20
SELECT
 apellido, oficio 
FROM
  emple 
WHERE
 dept_no=20
;

-- 20. Contar el numero de empleados cuyo oficio sea VENDEDOR
SELECT
 COUNT(*) 
FROM
  emple 
WHERE
 oficio LIKE 'VENDEDOR'
;

-- 21. Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente
SELECT
 * 
FROM
  emple 
WHERE
 apellido LIKE 'M%' OR apellido LIKE 'N%'
 ORDER BY apellido
;

-- 22. Seleccionar los empleados cuyo oficio sea 'VENDEDOR'. Mostrar los datos ordenados por apellido de forma ascendente
SELECT
 *
FROM
 emple
WHERE 
oficio LIKE 'VENDEDOR'
ORDER BY apellido
;

-- 23. Mostrar los apellidos del empleado que mas gana
SELECT
 apellido 
FROM
 emple 
WHERE
 salario=(SELECT MAX(salario) FROM emple)
;

-- 24. Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea 'ANALISTA'. Ordenar el resultado por apellido y oficio de forma ascendente
SELECT
 * 
FROM
 emple
WHERE
 dept_no=10
  AND
 oficio LIKE 'ANALISTA'
 ORDER BY apellido, oficio
;

-- 25. Realizar un listado de los distintos meses en que los empleados se han dado de alta
SELECT
  DISTINCT MONTH(fecha_alt) 
FROM 
  emple
  ORDER BY MONTH(fecha_alt)
;

-- 26. Realizar un listado de los distintos años en que los empleados se han dado de alta
SELECT
  DISTINCT YEAR(fecha_alt) 
FROM 
  emple
;

-- 27. Realizar un listado de los distintos dias del mes en que los empleados se han dado de alta
SELECT
  DISTINCT DAY(fecha_alt) 
FROM 
  emple
  ORDER BY DAY(fecha_alt)
;

-- 28. Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento numero 20
SELECT
 apellido 
FROM
 emple
WHERE
 dept_no=20
  OR
 salario>2000
;

-- 29. Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece
SELECT
  apellido, dnombre 
FROM
  emple
   JOIN depart USING(dept_no)
;

/* 30. Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento
al que pertenece. Ordenar los resultados por apellido de forma descendente */

SELECT
  apellido, oficio, dnombre 
FROM
  emple
   JOIN depart USING(dept_no)
ORDER BY
  apellido
;

/* 31. Listar el número de empleados por departamento.
  La salida del comando debe ser como la que vemos a continuación */
SELECT
  emple.dept_no DEPT_NO, COUNT(*) NUMERO_DE_EMPLEADOS 
FROM
  emple
  JOIN depart USING(dept_no)
GROUP BY dept_no
;

/* 32. Realizar el mismo comando anterior
  pero obteniendo una salida como la que vemos */
SELECT
  dnombre, COUNT(*) NUMERO_DE_EMPLEADOS 
FROM
  emple
  JOIN depart USING(dept_no)
GROUP BY dept_no
;

-- 33. Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre.
SELECT
  apellido
FROM
 emple
ORDER BY oficio, apellido
;

-- 34. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ. Listar el apellido de los empleados
SELECT
 apellido 
FROM
 emple
WHERE
 apellido LIKE 'A%'
;

-- 35. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. Listar el apellido de los empleados
SELECT
 apellido 
FROM
 emple
WHERE
 apellido LIKE 'A%'
  OR
 apellido LIKE 'M%'
;

/* 36. Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. Listar todos los campos de la tabla
empleados */
SELECT
 *
FROM
 emple
WHERE
 apellido NOT LIKE '%Z'
;

/* 37. Seleccionar de la tabla EMPLE aquellas filas cuyo APELLIDO empiece por ʻAʼ y el OFICIO tenga una ʻEʼ en cualquier
posición. Ordenar la salida por oficio y por salario de forma descendente */
SELECT
 * 
FROM
 emple
WHERE
 apellido LIKE 'A%'
  AND
 oficio LIKE '%e%'
ORDER BY
oficio, salario DESC
; 